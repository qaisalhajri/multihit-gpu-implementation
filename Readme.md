# Input 

Available is the ACC cancer dataset ``` ACC.maf2dat.matrix.out.noid ``` and the non-cancer sample dataset ``` all.mut ```

# Software

The code is organized in folders by the number of hits. 

The naming convention of the code is as follow:

``` <number of hit>hit_<optimization level abbreviation>.<file type>``` 

### Number of Hits

The number of hits currently tractable are 2-hits, 3-hits and 4-hits. Each is a directory that contains the various tractable optimization levels. 

- Note: 4-hit only contains the upper triangular matrix optimization since all other optimizations are intractable for 4-hits.

### File Types 

There are two types of files:

1. .c are C files that run on CPU
1. .cu are CUDA files that run on NVIDIA GPU

### Optimization Level

There are currently 6 optimization levels: 

| Optimization                     | Abbreviation |
|--- |--- |
| Matrix                           | mat |
| Compressed Binary                | cbin         |
| With Bound Check                 | bc            |
| Without Bound Check              | bcrm |
| 2-gene Combinations per Thread   | 2comb |
| Upper Triangular Matrix          | utm  |

#### Example

The source code that contains the upper triangular matrix gpu optimization to compute 4-hit combinations is:

``` 4hit/4hit_utm.cu ```


# Build

### CPU - C++

``` gcc <source file> -o <executable file>```

#### Example

To build 3-hit code with the *compressed binary* optimziation level on the CPU, execute the following:

``` cd 3hit/ ```

```gcc 3hit_cbin.c -o 3hit_cbin```

The executable in this case is ``` 3hit_cbin ```

### NVIDIA GPU - CUDA

``` nvcc <source file> -o <executable file> -arch=<gpu architecture> -code=<gpu architecture>```

#### Example

To build 3-hit code with the *without bound check* optimization level on the GPU, execute the following:

``` cd 3hit/```

```nvcc 3hit_bcrm.cu -o 3hit_bcrm -arch=compute_53 -code=compute_53``` 

The executable is ``` 3hit_bcrm ```

# Execution

``` ./<executable file> <Tumor matrix input data> <Normal matrix input data> <alpha ratio> ```

#### Example:

To identify 3-hit combinations for the ACC cancer dataset with equal sensitivity and specificity, execute the following: 

``` cd 3hit/ ```

```./3hit_bcrm ACC.maf2dat.matrix.out.noid all.mut 0.1``` 

- Note: 0.1 alpha ratio produces equal sensitivity and specificity. 

