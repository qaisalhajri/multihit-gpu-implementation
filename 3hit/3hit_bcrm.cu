
/***********************************************************************
 * fComb.c
 *
 * Identify multi-hit combinations based on LR-
 *
 * Calling Parameters: Tumor matrix file
 *                     Normal gene-sample list file
 * 
 * Output: list of 3-hit combinations
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <ctype.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <cuda.h>
#include "cuda_runtime.h"
static const int NAME_LEN  = 20;
#define NUM_BITS 64
#define gpuErrorCheck(ans) { gpuAssert((ans),__FILE__, __LINE__);}
inline void gpuAssert(cudaError_t code, const char * file, int line, bool abort=true) {
   if( code != cudaSuccess) {
      fprintf(stderr, "GPU Assert: %s %s %d \n", cudaGetErrorString(code), file, line);
      if( abort ) exit ( code );
   }
}

typedef struct {
   float f_max;
   int   gene1;
   int   gene2;
   int   gene3;
} multi_hit_comb; 


int count_high_bits_64( unsigned long long int num )
{
   int count = 0;
   for ( count; num; count++ ) 
   {
      num &= num - 1;
   }
   return count;
}
/***********************************************************************
 *
 * get count of unique genes and samples from header of input file
 *
 ************************************************************************/
void getNumGenesSamplesTumor( FILE *fp_gene_sample_matrix, int *num_genes, int *num_samples )
{
   int     i, j, ret_value;
   char    *line = NULL;
   size_t  len = 0;
   ssize_t read;

   /* First line contains number of genes and samples */
   read = getline( &line, &len, fp_gene_sample_matrix );
   ret_value = sscanf( line, "%d %d", &i, &j );
   if (ret_value == 2 )
   {
      *num_genes   = i;
      *num_samples = j;
   }
   else
   {
      printf("ERROR: invalid input file header %d\n", ret_value);
      exit( 1 );
   }
}


/***********************************************************************
 * Load gene-sample matrix data from input file
 *
 * Calling Parameters: gene-sample matrix input file
 *                     number of genes
 *                     number of samples
 *                     gene_sample matrix (updated)
 *
 ************************************************************************/
void loadGeneSampleMatrixTumor( FILE *fp_gene_sample_matrix, int num_genes, 
      int num_samples, int num_group_samples, unsigned long long int *gene_sample_matrix, 
      unsigned long long int *gene_bit_matrix, char *gene_id, int *tumor_samples_per_gene )
{
   int     i, j, k, n, ret_value;
   char    *line = NULL;
   char    *gene, *sample;
   size_t  len = 0;
   ssize_t read;

   gene   = (char *)malloc( NAME_LEN * sizeof( char ) );
   sample = (char *)malloc( NAME_LEN * sizeof( char ) );

   /* initialize matrix */
   for ( n = 0; n < num_genes; n++ )
   {
      tumor_samples_per_gene[n] = 0;
      for ( j = 0; j < num_group_samples; j++ )
      {
	 gene_sample_matrix[n * num_group_samples + j] = 0;
      }
      for ( j = 0; j < num_samples; j++ )
      {
	 gene_bit_matrix[n * num_samples + j]    = 0;
      }
   }

   while ( !feof( fp_gene_sample_matrix ) )
   {
      read = getline( &line, &len, fp_gene_sample_matrix );
      ret_value = sscanf( line, "%d %d %d %s %s", &i, &j, &k, gene, sample );
      if ( ret_value == 5 )
      {
	 strcpy( gene_id+(i*NAME_LEN), gene );
	 gene_bit_matrix[i * num_samples + j] = ( k > 0  ? 1 : 0 );
	 //gene_sample_matrix[i * num_samples + j] = k;
	 if ( k > 0 )
	 {
	    tumor_samples_per_gene[i]++;
	 }
      }
      else
      {
	 printf("ERROR: reading data from input file %d\n", ret_value);
	 exit( 1 );
      }
   }
   //FILE *fp;
   //fp = fopen("bitwise32_matrix.txt", "w");
   for ( n = 0; n < num_genes; n++ )
   {
      for ( j = 0; j < num_group_samples; j++ )
      {
	 for ( k = 0; k < NUM_BITS; k++ )
	 {
	    if ( num_samples > j * NUM_BITS + k ) 
	    {
	       gene_sample_matrix[n * num_group_samples + j] |=
		  ( (gene_bit_matrix[n * num_samples + ( j * NUM_BITS ) + k]) << (NUM_BITS - 1 - k) );
	       //fprintf(fp, "%llu", gene_bit_matrix[n * num_samples + (j * NUM_BITS) + k]);
	    }
	 }
	 // fprintf(fp, " ");
      }
      // fprintf(fp, "\n");
   }      

   return;

}


/***********************************************************************
 *
 * get count of samples in normal gene-sample file
 *
 ************************************************************************/
int getNumSamplesNormal( FILE *fp_gene_sample_list )
{
   int     num_samples, last_sample;
   int     sample, ret_value;
   char    gene[NAME_LEN];
   char    *line = NULL;
   size_t  len = 0;
   ssize_t read;
   num_samples = last_sample = 0;

   /* read to end of file */
   while ( !feof( fp_gene_sample_list ) )
   {
      read = getline( &line, &len, fp_gene_sample_list );
      ret_value = sscanf( line, "%s %d", gene, &sample );
      if (ret_value == 2 )
      {
	 if ( sample != last_sample )
	 {
	    num_samples++;
	    last_sample = sample;
	 }
      }
      else
      {
	 printf("ERROR: invalid line in normal gene-sample list %d\n", ret_value);
	 exit( 1 );
      }
   }
   rewind( fp_gene_sample_list );

   return( num_samples );
}


/***********************************************************************
 * Load gene-sample matrix data from input file
 *
 * Calling Parameters: gene-sample matrix input file
 *                     number of genes
 *                     number of samples
 *                     gene_sample matrix (updated)
 *
 ************************************************************************/
void loadGeneSampleMatrixNormal( FILE *fp_gene_sample_list, int num_genes, 
      int num_samples_normal, int num_group_samples_normal, unsigned long long int *normal_matrix,
      unsigned long long int *normal_bit_matrix, char *gene_id, int *normal_samples_per_gene )
{
   int     j, n, k, ret_value;
   char    *line = NULL;
   char    gene[NAME_LEN];
   int     sample, new_sample_id, matrix_sample_index;
   size_t  len = 0;
   ssize_t read;
   int     *sample_id; /* to translate from sample# in file to sequential # */
   int     max_samples = 1000; /* for allocation of sample_id list */

   /* list of old to new (sequential excluding missing numbers) sample ids */
   sample_id = (int *)malloc( max_samples * sizeof( int ) );
   if ( sample_id == NULL )
   {
      printf( "ERROR: failed to allocate memory for normal gene_sample_matrix \n" );
      exit( 1 );
   }
   for ( j = 0; j < max_samples; j++ )
   {
      sample_id[j] = -1;
   }

   /* initialize matrix */
   for ( n = 0; n < num_genes; n++ )
   {
      normal_samples_per_gene[n] = 0;
      for ( j = 0; j < num_samples_normal; j++ )
      {
	 normal_bit_matrix[n * num_samples_normal + j] = 0;
      }
      for ( j = 0; j < num_group_samples_normal; j++ )
      {
	 normal_matrix[n * num_group_samples_normal + j] = 0;
      }
   }

   new_sample_id = 0;
   while ( !feof( fp_gene_sample_list ) )
   {
      read = getline( &line, &len, fp_gene_sample_list );
      ret_value = sscanf( line, "%s %d", gene, &sample );
      if ( ret_value == 2 )
      {
	 if ( sample_id[sample] < 0 )
	 {
	    sample_id[sample] = new_sample_id;
	    matrix_sample_index = new_sample_id;
	    new_sample_id++;
	 }
	 else
	 {
	    matrix_sample_index = sample_id[sample];
	 }
	 for ( n = 0; n < num_genes; n++ )
	 {
	    if ( strcmp( gene_id+(n*NAME_LEN), gene ) == 0 )
	    {
	       normal_bit_matrix[n * num_samples_normal + matrix_sample_index] = 1;
	       normal_samples_per_gene = 0;
	       break;
	    }
	 }
      }
      else
      {
	 printf("ERROR: reading data from normal gene-sample input file %d\n", ret_value);
	 exit( 1 );
      }
   }
   for ( n = 0; n < num_genes; n++ )
   {
      for ( j = 0; j < num_group_samples_normal; j++ ) 
      {
	 for ( k = 0; k < NUM_BITS; k++ )
	 {
	    if ( num_samples_normal > (j * NUM_BITS + k) )
	    {
	       normal_matrix[n * num_group_samples_normal + j] |= 
		  ( (normal_bit_matrix[n * num_samples_normal + ( j * NUM_BITS ) + k]) << (NUM_BITS - 1 - k) );
	    }
	 }
      }
   }
   free( sample_id );
}


/***********************************************************************
 * Find combination with smallest lr- 
 *
 * Calling Parameters: tumor gene-sample matrix
 *                     number of genes
 *                     number of samples
 *                     normal gene-sample matrix
 *                     tumor genes_per_sample for lr bound
 * 
 ************************************************************************/
__global__ void maxF( unsigned long long int *tumor_matrix, int num_genes, int num_samples_tumor,
      int num_group_samples_tumor, unsigned long long int *normal_matrix, int num_samples_normal,
      int num_group_samples_normal, int *tumor_samples_per_gene, int *normal_samples_per_gene,
      unsigned long long int *excluded_samples, multi_hit_comb *comb, float beta )
{
   int   i1, i2, i3, j, count;
   int   true_pos, false_pos, true_neg, false_neg;
   float f;          /* f-measure */
   float f_bound;
   int   num_skipped;
   int   temp_tp, temp_tp2, temp_tp3;
   unsigned long long int bit_true_pos, bit_false_pos;
   num_skipped = 0;
   i1 = blockDim.x * blockIdx.x + threadIdx.x;
   if ( i1 >= num_genes )
      return;
   comb[i1].f_max = 0.0;
   for ( i2 = i1+1; i2 < num_genes; i2++ )
   {
      for ( i3 = i2 + 1; i3 < num_genes; i3++ ) 
      {
	 true_pos = 0;
	 for ( j = 0; j < num_group_samples_tumor; j++ )
	 {
	    count = 0;	    
	    bit_true_pos = ( ~excluded_samples[j] ) 
	       & tumor_matrix[i1 * num_group_samples_tumor + j]
	       & tumor_matrix[i2 * num_group_samples_tumor + j] 
	       & tumor_matrix[i3 * num_group_samples_tumor + j];
	    true_pos += __popcll(bit_true_pos);
	 }
	 false_pos = 0;
	 for ( j = 0; j < num_group_samples_normal; j++ )
	 {
	    count = 0;
	    bit_false_pos = normal_matrix[i1 * num_group_samples_normal + j]
	       & normal_matrix[i2 * num_group_samples_normal + j] 
	       & normal_matrix[i3 * num_group_samples_normal + j];
	    false_pos += __popcll( bit_false_pos);
	 }
	 false_neg = num_samples_tumor  - true_pos;
	 true_neg  = num_samples_normal - false_pos; 
	 if( true_pos > 0 ) {
	    f         = (float)(beta * (float) true_pos + (float) true_neg) / (float)(num_samples_tumor + num_samples_normal);
	    if(f > comb[i1].f_max) {
	       comb[i1].f_max = f;
	       comb[i1].gene1 = i1;
	       comb[i1].gene2 = i2;
	       comb[i1].gene3 = i3;
	    }
	 }
      }
   }
}

/***********************************************************************
 * exclude samples contatining gene combinations found
 *
 * Calling Parameters: gene index
 *                     excluded samples list (for update)
 *                     tumor gene-sample matrix
 *                     number of samples
 * 
 ************************************************************************/
int excludeSamples( multi_hit_comb g, unsigned long long int *excluded_samples, unsigned long long int *tumor_matrix, int num_group_samples_tumor ) 
{
   int   s, num_excluded;
   unsigned long long int num_excluded_bits;
   num_excluded = 0;
   for ( s = 0; s < num_group_samples_tumor; s++ )
   {
      num_excluded_bits = ( ~excluded_samples[s] ) 
	 & tumor_matrix[g.gene1 * num_group_samples_tumor + s]
	 & tumor_matrix[g.gene2 * num_group_samples_tumor + s] 
	 & tumor_matrix[g.gene3 * num_group_samples_tumor + s];
      num_excluded += count_high_bits_64( num_excluded_bits );
      excluded_samples[s] |= tumor_matrix[g.gene1 * num_group_samples_tumor + s] 
	 & tumor_matrix[g.gene2 * num_group_samples_tumor + s] 
	 & tumor_matrix[g.gene3 * num_group_samples_tumor + s]; 
   }
   return( num_excluded );
}
/****************************************************************************
 * warp reduce to maximize use of the GPU
 *
 * Calling Paramters: shared data
 *                    thread index
 *****************************************************************************/
__device__ void warpMaxReduction( volatile multi_hit_comb* sdata, int tid ) {
   if ( sdata[tid + 32].f_max > sdata[tid].f_max ) {
      sdata[tid].f_max = sdata[tid + 32].f_max;
      sdata[tid].gene1 = sdata[tid + 32].gene1;
      sdata[tid].gene1 = sdata[tid + 32].gene2;
   }
   if ( sdata[tid + 16].f_max > sdata[tid].f_max ) {
      sdata[tid].f_max = sdata[tid + 16].f_max;
      sdata[tid].gene1 = sdata[tid + 16].gene1;
      sdata[tid].gene2 = sdata[tid + 16].gene2;
   }
   if ( sdata[tid + 8 ].f_max > sdata[tid].f_max ) {
      sdata[tid].f_max = sdata[tid + 8].f_max;
      sdata[tid].gene1 = sdata[tid + 8].gene1;
      sdata[tid].gene2 = sdata[tid + 8].gene2;
   }
   if ( sdata[tid + 4].f_max > sdata[tid].f_max ) {
      sdata[tid].f_max = sdata[tid + 4].f_max;
      sdata[tid].gene1 = sdata[tid + 4].gene1;
      sdata[tid].gene2 = sdata[tid + 4].gene2;
   }
   if ( sdata[tid + 2].f_max > sdata[tid].f_max ) {
      sdata[tid].f_max = sdata[tid + 2].f_max;
      sdata[tid].gene1 = sdata[tid + 2].gene1;
      sdata[tid].gene2 = sdata[tid + 2].gene2;
   }
   if ( sdata[tid + 1].f_max > sdata[tid].f_max ) {
      sdata[tid].f_max = sdata[tid + 1].f_max;
      sdata[tid].gene1 = sdata[tid + 1].gene1;
      sdata[tid].gene2 = sdata[tid + 1].gene2;
   }
}
/*************************************************************************
 * parallel reduction to find the maximum f_max in O(logn) time
 * 
 * Calling Parameters: two hit combination structure input
 *                     two hit combination structure output
 *
 ************************************************************************/
__global__ void parallelReduceMax(multi_hit_comb *comb_in, multi_hit_comb *comb_out, int num_genes ) {
   extern  __shared__ multi_hit_comb shared_data[];
   unsigned int tid = threadIdx.x;
   unsigned int i   = blockIdx.x *( blockDim.x) + threadIdx.x;
   multi_hit_comb temp;
   temp.f_max = 0.0;
   temp.gene1 = 0;
   temp.gene2 = 0;
   temp.gene3 = 0;
   if ( i < num_genes ) 
      temp = comb_in[i];
   shared_data[tid] = temp;
   __syncthreads();
   for ( unsigned int s = blockDim.x/2; s > 0; s >>= 1) {
      if (tid < s) {
	 if( shared_data[tid + s].f_max > shared_data[tid].f_max) {
	    shared_data[tid] = shared_data[tid + s];
	 }
      }
      __syncthreads();
   }
   //if ( tid < 32 ) 
   // warpMaxReduction( shared_data, tid );
   /*if( tid < 32 ) {
     if ( shared_data[tid + 32].f_max > shared_data[tid].f_max ) shared_data[tid] = shared_data[tid + 32]; __syncthreads();
     if ( shared_data[tid + 16].f_max > shared_data[tid].f_max ) shared_data[tid] = shared_data[tid + 16]; __syncthreads();
     if ( shared_data[tid + 8 ].f_max > shared_data[tid].f_max ) shared_data[tid] = shared_data[tid + 8 ]; __syncthreads();
     if ( shared_data[tid + 4 ].f_max > shared_data[tid].f_max ) shared_data[tid] = shared_data[tid + 4 ]; __syncthreads();
     if ( shared_data[tid + 2 ].f_max > shared_data[tid].f_max ) shared_data[tid] = shared_data[tid + 2 ]; __syncthreads();
     if ( shared_data[tid + 1 ].f_max > shared_data[tid].f_max ) shared_data[tid] = shared_data[tid + 1 ]; __syncthreads();
     }*/
   if (tid == 0 ){
      comb_out[blockIdx.x] = shared_data[0];
   }
}
/***********************************************************************
 * list all combinations found in gene-sample matrix
 *
 * Calling Parameters: tumor gene-sample matrix 
 *                     number of genes
 *                     number of samples (tumor)
 *                     normal gene-sample matrix
 *                     number of samples (normal)
 *                     gene_id (name list)
 *                     tumor samples per gene 
 * 
 ************************************************************************/
int listCombs( unsigned long long int *tumor_matrix, int num_genes, int num_samples_tumor, int num_group_samples_tumor, unsigned long long int *normal_matrix, int num_samples_normal, int num_group_samples_normal, char *gene_id, int *tumor_samples_per_gene, int *normal_samples_per_gene, float beta )
{
   int   i, num_found, num_excluded, tot_excluded;
   char *gene1_name, *gene2_name, *gene3_name;
   unsigned long long int  *excluded_samples, *excluded_samples_d;
   int threadsPerBlock, blocksPerGrid, sharedMemSize;
   multi_hit_comb *comb, *comb_d, *comb_d_result;

   unsigned long long int *tumor_matrix_dev;
   unsigned long long int *normal_matrix_dev;
   int          *tumor_samples_per_gene_dev;
   int          *normal_samples_per_gene_dev;
   comb 	    = (multi_hit_comb *)malloc( sizeof( multi_hit_comb ) * num_genes ); 
   gene1_name       = (char *)malloc( NAME_LEN          * sizeof( char ) );
   gene2_name       = (char *)malloc( NAME_LEN          * sizeof( char ) );
   gene3_name       = (char *)malloc( NAME_LEN          * sizeof( char ) );
   excluded_samples = (unsigned long long int  *)malloc( num_group_samples_tumor * sizeof( unsigned long long int ) );
   gpuErrorCheck( cudaMalloc( &comb_d ,       sizeof( multi_hit_comb ) * num_genes ) );
   gpuErrorCheck( cudaMalloc( &comb_d_result, sizeof( multi_hit_comb ) * num_genes ) ); 
   gpuErrorCheck( cudaMalloc( &tumor_matrix_dev,  num_genes * num_group_samples_tumor * sizeof( unsigned long long int ) ) );
   gpuErrorCheck( cudaMalloc( &normal_matrix_dev, num_genes * num_group_samples_normal* sizeof( unsigned long long int ) ) );
   gpuErrorCheck( cudaMalloc( &tumor_samples_per_gene_dev,  num_genes * sizeof( int ) ) );
   gpuErrorCheck( cudaMalloc( &normal_samples_per_gene_dev, num_genes * sizeof( int ) ) );
   gpuErrorCheck( cudaMalloc( &excluded_samples_d, num_group_samples_tumor * sizeof( unsigned long long int ) ) );
   gpuErrorCheck( cudaMemcpy( tumor_matrix_dev,  tumor_matrix,  num_genes * num_group_samples_tumor  * sizeof( unsigned long long int ), cudaMemcpyHostToDevice ) );
   gpuErrorCheck( cudaMemcpy( normal_matrix_dev, normal_matrix, num_genes * num_group_samples_normal * sizeof( unsigned long long int ), cudaMemcpyHostToDevice ) );
   gpuErrorCheck( cudaMemcpy( tumor_samples_per_gene_dev,  tumor_samples_per_gene,  num_genes * sizeof( int ), cudaMemcpyHostToDevice ) );
   gpuErrorCheck( cudaMemcpy( normal_samples_per_gene_dev, normal_samples_per_gene, num_genes * sizeof( int ), cudaMemcpyHostToDevice ) );
   if ( excluded_samples == NULL )
   {
      printf( "ERROR: failed to allocate memory for excluded_samples \n" );
      exit( 1 );
   }
   for ( i = 0; i < num_group_samples_tumor; i++ )
   {
      excluded_samples[i] = 0;
   }
   threadsPerBlock = 256;
   blocksPerGrid = (num_genes + threadsPerBlock - 1) / threadsPerBlock;
   sharedMemSize = threadsPerBlock * sizeof(multi_hit_comb);
   num_found    = 0;
   tot_excluded = 0;
   i = 1;
   while ( tot_excluded < num_samples_tumor )
   {
      gpuErrorCheck( cudaMemcpy( excluded_samples_d, excluded_samples, num_group_samples_tumor * sizeof ( unsigned long long int ), cudaMemcpyHostToDevice ) );
      maxF<<< blocksPerGrid, threadsPerBlock >>>( tumor_matrix_dev, num_genes, num_samples_tumor, num_group_samples_tumor, normal_matrix_dev, 
	    num_samples_normal, num_group_samples_normal, tumor_samples_per_gene_dev, 
	    normal_samples_per_gene_dev, excluded_samples_d, comb_d, beta );
      gpuErrorCheck( cudaGetLastError() );
      gpuErrorCheck( cudaDeviceSynchronize() );
      parallelReduceMax<<< blocksPerGrid, threadsPerBlock, sharedMemSize>>>( comb_d, comb_d_result, num_genes );
      gpuErrorCheck( cudaGetLastError() );
      gpuErrorCheck( cudaDeviceSynchronize() );
      int num = num_genes;
      while(num > 1) {
	 parallelReduceMax<<< blocksPerGrid, threadsPerBlock, sharedMemSize>>>( comb_d_result, comb_d_result, num_genes );
	 gpuErrorCheck( cudaGetLastError() );
	 gpuErrorCheck( cudaDeviceSynchronize() );
	 num = (num + threadsPerBlock - 1)/threadsPerBlock;
      }
      gpuErrorCheck( cudaMemcpy(comb , comb_d_result, sizeof( multi_hit_comb  ) * num_genes, cudaMemcpyDeviceToHost) );
      num_excluded = excludeSamples( comb[0], excluded_samples, tumor_matrix, num_group_samples_tumor );
      tot_excluded += num_excluded;
      num_found++;

      strcpy( gene1_name, gene_id+((comb[0].gene1)*NAME_LEN) );
      strcpy( gene2_name, gene_id+((comb[0].gene2)*NAME_LEN) );
      strcpy( gene3_name, gene_id+((comb[0].gene3)*NAME_LEN) );
      printf( "%d- %s %s %s %d %d %d F-max = %9.6f , num excluded %d, tot excluded %d \n", 
	    i, gene1_name, gene2_name, gene3_name, comb[0].gene1, comb[0].gene2, comb[0].gene3, comb[0].f_max, num_excluded, tot_excluded);

      if ( num_excluded == 0 )
      {
	 break;
      }
      i++;
   }
   free(comb);
   free(gene1_name);
   free(gene2_name);
   free(gene3_name);
   free(excluded_samples);
   cudaFree(comb_d);
   cudaFree(comb_d_result);
   cudaFree(tumor_matrix_dev);
   cudaFree(normal_matrix_dev);
   cudaFree(excluded_samples_d);
   cudaFree(tumor_samples_per_gene_dev);
   cudaFree(normal_samples_per_gene_dev);
   return( num_found );
}


/*****************************************************************************************
 * main()                                                                                *
 *                                                                                       *
 * calculate ratio of occurunce os multi-hit gene combinations                           *
 *                                                                                       *
 * Calling Parameters: list of tumor and normal gene-sample counts,                      *
 *                     list of freq mutated genes                                        *
 *                                                                                       *
 * output: ratio of occurance of multi-hit gene combinations in normal and tumor samples *
 *****************************************************************************************/
int main(int argc, char ** argv)
{
   int            		num_genes, num_samples, num_samples_normal;   /* number of genes and samples in tcga maf data */
   int            		num_group_samples, num_group_samples_normal;
   int            		num_comb;                 /* number of 3-hit combinations found in tcga samples */
   unsigned long long int       *tumor_matrix;            /* matrix of genesxsamples */
   unsigned long long int       *normal_matrix;           /* matrix of genesxsamples */
   unsigned long long int           *tumor_bit_matrix;
   unsigned long long int           *normal_bit_matrix;
   int            		*tumor_samples_per_gene;  /* total True Pos for calulating bound */
   int            		*normal_samples_per_gene;  /* total False Pos for calulating bound */
   FILE           		*fp_tumor_matrix;
   FILE           		*fp_normal_matrix;
   char           		*gene_id;                 /* list of gene ids */
   float          		beta;

   if (argc != 4)
   {
      printf("ERROR: fComb requires 3 parameters\n");
      printf("       - Tumor gene-sample count matrix file (training)\n");
      printf("       - Normal gene-sample list file (training)\n");
      printf("       - Beta value (Fbeta-score)\n");
      exit(1);
   }

   if ( ( fp_tumor_matrix = fopen( argv[1], "r" ) ) == NULL )
   {
      printf( "ERROR: unable to open tumor gene-sample count matrix file %s, \n", argv[2] );
      exit( 1 );
   }

   if ( ( fp_normal_matrix = fopen( argv[2], "r" ) ) == NULL )
   {
      printf( "ERROR: unable to open normal gene-sample count matrix file %s, \n", argv[3] );
      exit( 1 );
   }

   beta = atof( argv[3] );

   /* load tumor gene-sample matrix */
   getNumGenesSamplesTumor( fp_tumor_matrix, &num_genes, &num_samples );
   printf( "Num Tumor genes = %d tumor samples = %d \n", num_genes, num_samples );
   num_group_samples = ( num_samples / NUM_BITS ) + 1;
   tumor_matrix = (unsigned long long int  *)malloc( num_genes * num_group_samples * sizeof( unsigned long long int ) );
   if ( tumor_matrix == NULL )
   {
      printf( "ERROR: failed to allocate memory for tumor gene_sample_matrix \n" );
      exit( 1 );
   }
   tumor_bit_matrix = (unsigned long long int *)malloc( num_genes * num_samples * sizeof( unsigned long long int ) );
   if ( tumor_bit_matrix  == NULL )
   {
      printf( "ERROR: failed to allocate memory for tumor bit matrix \n" );
      exit( 1 );
   }

   gene_id = (char  *)malloc( num_genes * NAME_LEN * sizeof( char ));
   if ( gene_id == NULL )
   {
      printf( "ERROR: failed to allocate memory for gene_ids \n" );
      exit( 1 );
   }
   tumor_samples_per_gene = (int  *)malloc( num_genes * sizeof( int ) );
   if ( tumor_samples_per_gene == NULL )
   {
      printf( "ERROR: failed to allocate memory for tumor samples per gene \n" );
      exit( 1 );
   }
   loadGeneSampleMatrixTumor( fp_tumor_matrix, num_genes, num_samples, 
	 num_group_samples, tumor_matrix, tumor_bit_matrix, gene_id, tumor_samples_per_gene );

   fclose( fp_tumor_matrix );

   /* load normal gene-sample matrix */
   num_samples_normal = getNumSamplesNormal( fp_normal_matrix );
   printf( "Num normal samples = %d \n", num_samples_normal );
   num_group_samples_normal = ( num_samples_normal / NUM_BITS ) + 1;

   normal_matrix = ( unsigned long long int *)malloc( num_genes * num_group_samples_normal * sizeof( unsigned long long int ) );
   if ( normal_matrix == NULL )
   {
      printf( "ERROR: failed to allocate memory for normal gene_sample_matrix \n" );
      exit( 1 );
   }
   normal_bit_matrix = (unsigned long long int *)malloc( num_genes * num_samples_normal * sizeof( unsigned long long int ) );
   if ( normal_bit_matrix == NULL )
   {
      printf( "ERROR: failed to allocate memory for normal bit matrix \n");
      exit( 1 );
   }

   normal_samples_per_gene = (int  *)malloc( num_genes * sizeof( int ) );
   if ( normal_samples_per_gene == NULL )
   {
      printf( "ERROR: failed to allocate memory for normal samples per gene \n" );
      exit( 1 );
   }
   loadGeneSampleMatrixNormal( fp_normal_matrix, num_genes, num_samples_normal,
	 num_group_samples_normal, normal_matrix, normal_bit_matrix, gene_id, normal_samples_per_gene );

   fclose( fp_normal_matrix );

   /* Check combinations of genes for coverage of samples */
   num_comb = listCombs( tumor_matrix, num_genes, num_samples, num_group_samples, 
	 normal_matrix, num_samples_normal, num_group_samples_normal, gene_id, 
	 tumor_samples_per_gene, normal_samples_per_gene, beta );
   printf( "Num 3-hit combinations = %d  (beta = %f )\n", num_comb, beta );

   free( tumor_matrix );
   free( normal_matrix );
   free( gene_id );
   free( tumor_samples_per_gene );
   free( normal_samples_per_gene );

   return( 0 );
}

